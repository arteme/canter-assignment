'use strict';

/* App Module */

var pimApp = angular.module('myApp', [
  'ui.router',
  'ngResource',
  'ngMaterial',
  'md.data.table',
  'ui.bootstrap',
  'ngToast',
  'pimControllers',
  'pimServices'
]);

pimApp.config(['$stateProvider', '$urlRouterProvider',
  function($stateProvider, $urlRouterProvider) {

      $stateProvider.state('list', {
          url: '/list',
          templateUrl: 'templates/list.html',
          controller: 'ProductListCtrl'
      });
      $stateProvider.state('create', {
          url: '/create',
          templateUrl: 'templates/edit.html',
          controller: 'ProductEditCtrl'
      });
      $stateProvider.state('edit', {
          url: '/edit/:id',
          templateUrl: 'templates/edit.html',
          controller: 'ProductEditCtrl'
      });

      $urlRouterProvider.when('', '/list');

  }]);