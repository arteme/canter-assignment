'use strict';

/* */
var pimServices = angular.module('pimServices', []);

pimServices.factory('Product', function ($resource) {
   var res = $resource('/products/:id', {id: '@id'}, {
       create: { method: 'POST' },
       update: { method: 'PUT' }
   });
   // Patch $save() function to be smart about calling either $create or $update
   res.prototype.$save = function (params, success, error) {
       if (this.id) {
           return this.$update(params, success, error);
       } else {
           return this.$create(params, success, error);
       }
   };
   return res;
});

/* Controllers */

var pimControllers = angular.module('pimControllers', []);

pimControllers.controller('ProductListCtrl', ['$scope', '$state', 'Product',
  function($scope, $state, Product) {

    $scope.products = Product.query();

    $scope.delete = function (index) {
        var id = $scope.products[index].id;
        Product.delete({id: id}, function () {
            $scope.products.splice(index, 1);
        });
    };

    $scope.edit = function (index) {
        var id = $scope.products[index].id;
        $state.go('edit', {id: id})
    };

    $scope.add = function () {
        $state.go('create')
    };

  }]);

pimControllers.controller('ProductEditCtrl', ['$scope', '$state', '$stateParams', 'ngToast', 'Product',
  function($scope, $state, $stateParams, ngToast, Product) {

    if(angular.isUndefined($stateParams.id)) {
        $scope.product = new Product({details: []});
    } else {
        $scope.product = Product.get({id: $stateParams.id});
    }

    function onSaveSuccess() {
        ngToast.success({
            content: 'Product saved successfully'
        });
    }

    function onSaveFailure() {
        ngToast.danger({
            content: 'Product saving failed!'
        });
    }

    $scope.save = function () {
        $scope.product.$save(null, onSaveSuccess, onSaveFailure);
    }

    $scope.addDetail = function () {
        if(angular.isUndefined($scope.product.details)) {
            $scope.product.details = [];
        }
        $scope.product.details.push({key: $scope.addDetailKey, value: $scope.addDetailValue});
        delete $scope.addDetailKey;
        delete $scope.addDetailValue;
    }

    $scope.removeDetail = function (index) {
        $scope.product.details.splice(index, 1);
    }

  }]);