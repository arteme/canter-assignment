
libraryDependencies ++= Seq(
  "com.typesafe.play" %% "play-slick" % "3.0.1",
  "org.mariadb.jdbc" % "mariadb-java-client" % "1.3.7",

  jdbc , ehcache , ws , specs2 % Test , guice
)

unmanagedResourceDirectories in Test <+=  baseDirectory ( _ /"target/web/public/test" )  

      