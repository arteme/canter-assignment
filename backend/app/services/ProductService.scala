package services

import db.Tables
import javax.inject.{Inject, Singleton}
import models.ProductModels
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class ProductService @Inject() (val Products: db.Products,
                                val Details: db.Details,
                                protected val dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext)
  extends HasDatabaseConfigProvider[JdbcProfile] {

    import dbConfig.profile.api._

  private def toDetailModel(d: Tables.DetailRow) = ProductModels.Detail(d.key, d.value)

  private def toProductModel(p: Tables.ProductRow, d: Seq[Tables.DetailRow]) =
      ProductModels.Product(Some(p.id), p.name, p.category, p.code, d.map(toDetailModel).toList, p.price)

  def create(product: ProductModels.Product) = {
    val action =
      // create the product row
      Products.create(Tables.ProductRow(0, product.name, product.category, product.code, product.price)) flatMap { productId: Int =>
        // create detail rows
        DBIO.sequence(product.details.map { detail =>
          Details.create(Tables.DetailRow(0, productId, detail.key, detail.value))
        }) andThen
          // return updated product model instance
          DBIO.successful(product.copy(id = Some(productId)))
      }

    db.run(action.transactionally)
  }

  def update(product: ProductModels.Product) = {
    val update =
      Products.update(Tables.ProductRow(product.id.get, product.name, product.category, product.code, product.price)) map {
        case 1 => product
        case 0 => throw new RuntimeException("Update failed")
      }

    val action =
      // delete detail rows
      Details.deleteForProductId(product.id.get) andThen
        // create detail rows
        DBIO.sequence(product.details.map { detail =>
          Details.create(Tables.DetailRow(0, product.id.get, detail.key, detail.value))
        }) andThen
        // update product row
        update

    db.run(action.transactionally)
  }

  def list = {
    // first get the product list and lookup the details for each product
    val mapped = for {
      ps  <- Products.list
      dss <- Future.sequence(ps map { p => Details.forProductId(p.id) })
    } yield ps zip dss
    // convert Future[Seq[(product row, detail rows)]] to Future[Seq[product model]]
    mapped map { _.map { case (p, ds) => toProductModel(p, ds) } }
  }

  def get(id: Int) = {
    for {
      p <- Products.get(id)
      ds <- Details.forProductId(id)
    } yield p.map { p => toProductModel(p, ds) }
  }

  def delete(id: Int) = {
    Products.delete(id)
  }

}
