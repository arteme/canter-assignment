package models

import play.api.libs.json.Json

object ProductModels {

 case class Detail(key: String, value: String)
 case class Product(id: Option[Int], name: String, category: String, code: String, details: List[Detail], price: BigDecimal)

 implicit val detailFormat = Json.format[Detail]
 implicit val productFormat = Json.format[Product]

}
