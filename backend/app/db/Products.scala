package db

import javax.inject._
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class Products @Inject() (protected val dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) extends HasDatabaseConfigProvider[JdbcProfile] {

  import dbConfig.profile.api._

  def create(product: Tables.ProductRow) = {
    Tables.Product returning Tables.Product.map(_.id) += product
  }

  def update(product: Tables.ProductRow) = {
    Tables.Product.filter { _.id === product.id }
      .map { r => (r.name, r.category, r.code, r.price) }
      .update((product.name, product.category, product.code, product.price))
  }

  def list: Future[Seq[Tables.ProductRow]] = {
    db.run(Tables.Product.result)
  }

  def get(id: Int) = {
    db.run(Tables.Product.filter { _.id === id }.result.headOption)
  }

  def delete(id: Int) = {
    db.run(Tables.Product.filter { _.id === id }.delete)
  }

}
