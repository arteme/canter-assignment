package db
// AUTO-GENERATED Slick data model
/** Stand-alone Slick data model for immediate use */
object Tables extends {
  val profile = slick.jdbc.MySQLProfile
} with Tables

/** Slick data model trait for extension, choice of backend or usage in the cake pattern. (Make sure to initialize this late.) */
trait Tables {
  val profile: slick.jdbc.JdbcProfile
  import profile.api._
  import slick.model.ForeignKeyAction
  // NOTE: GetResult mappers for plain SQL are only generated for tables where Slick knows how to map the types of all columns.
  import slick.jdbc.{GetResult => GR}

  /** DDL for all tables. Call .create to execute. */
  lazy val schema: profile.SchemaDescription = Detail.schema ++ Product.schema
  @deprecated("Use .schema instead of .ddl", "3.0")
  def ddl = schema

  /** Entity class storing rows of table Detail
   *  @param id Database column id SqlType(INT), AutoInc, PrimaryKey
   *  @param productId Database column product_id SqlType(INT)
   *  @param key Database column key SqlType(VARCHAR), Length(128,true)
   *  @param value Database column value SqlType(VARCHAR), Length(128,true) */
  final case class DetailRow(id: Int, productId: Int, key: String, value: String)
  /** GetResult implicit for fetching DetailRow objects using plain SQL queries */
  implicit def GetResultDetailRow(implicit e0: GR[Int], e1: GR[String]): GR[DetailRow] = GR{
    prs => import prs._
    DetailRow.tupled((<<[Int], <<[Int], <<[String], <<[String]))
  }
  /** Table description of table detail. Objects of this class serve as prototypes for rows in queries. */
  class Detail(_tableTag: Tag) extends profile.api.Table[DetailRow](_tableTag, Some("canter"), "detail") {
    def * = (id, productId, key, value) <> (DetailRow.tupled, DetailRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(productId), Rep.Some(key), Rep.Some(value)).shaped.<>({r=>import r._; _1.map(_=> DetailRow.tupled((_1.get, _2.get, _3.get, _4.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(INT), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column product_id SqlType(INT) */
    val productId: Rep[Int] = column[Int]("product_id")
    /** Database column key SqlType(VARCHAR), Length(128,true) */
    val key: Rep[String] = column[String]("key", O.Length(128,varying=true))
    /** Database column value SqlType(VARCHAR), Length(128,true) */
    val value: Rep[String] = column[String]("value", O.Length(128,varying=true))

    /** Foreign key referencing Product (database name fk__product_id) */
    lazy val productFk = foreignKey("fk__product_id", productId, Product)(r => r.id, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Cascade)

    /** Uniqueness Index over (productId,key) (database name idx__product_and_key) */
    val index1 = index("idx__product_and_key", (productId, key), unique=true)
  }
  /** Collection-like TableQuery object for table Detail */
  lazy val Detail = new TableQuery(tag => new Detail(tag))

  /** Entity class storing rows of table Product
   *  @param id Database column id SqlType(INT), AutoInc, PrimaryKey
   *  @param name Database column name SqlType(VARCHAR), Length(128,true)
   *  @param category Database column category SqlType(VARCHAR), Length(128,true)
   *  @param code Database column code SqlType(VARCHAR), Length(16,true)
   *  @param price Database column price SqlType(DECIMAL) */
  final case class ProductRow(id: Int, name: String, category: String, code: String, price: scala.math.BigDecimal)
  /** GetResult implicit for fetching ProductRow objects using plain SQL queries */
  implicit def GetResultProductRow(implicit e0: GR[Int], e1: GR[String], e2: GR[scala.math.BigDecimal]): GR[ProductRow] = GR{
    prs => import prs._
    ProductRow.tupled((<<[Int], <<[String], <<[String], <<[String], <<[scala.math.BigDecimal]))
  }
  /** Table description of table product. Objects of this class serve as prototypes for rows in queries. */
  class Product(_tableTag: Tag) extends profile.api.Table[ProductRow](_tableTag, Some("canter"), "product") {
    def * = (id, name, category, code, price) <> (ProductRow.tupled, ProductRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(name), Rep.Some(category), Rep.Some(code), Rep.Some(price)).shaped.<>({r=>import r._; _1.map(_=> ProductRow.tupled((_1.get, _2.get, _3.get, _4.get, _5.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(INT), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column name SqlType(VARCHAR), Length(128,true) */
    val name: Rep[String] = column[String]("name", O.Length(128,varying=true))
    /** Database column category SqlType(VARCHAR), Length(128,true) */
    val category: Rep[String] = column[String]("category", O.Length(128,varying=true))
    /** Database column code SqlType(VARCHAR), Length(16,true) */
    val code: Rep[String] = column[String]("code", O.Length(16,varying=true))
    /** Database column price SqlType(DECIMAL) */
    val price: Rep[scala.math.BigDecimal] = column[scala.math.BigDecimal]("price")
  }
  /** Collection-like TableQuery object for table Product */
  lazy val Product = new TableQuery(tag => new Product(tag))
}
