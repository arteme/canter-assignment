package db

import javax.inject._
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class Details @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) extends HasDatabaseConfigProvider[JdbcProfile] {

  import dbConfig.profile.api._

  def create(detail: Tables.DetailRow) = {
    Tables.Detail returning Tables.Detail.map(_.id) += detail
  }

  def forProductId(productId: Int) = {
    db.run(Tables.Detail.filter { _.productId === productId }.result)
  }

  def deleteForProductId(productId: Int) = {
    Tables.Detail.filter { _.productId === productId }.delete
  }

}
