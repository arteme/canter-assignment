package controllers

import javax.inject._
import models.ProductModels
import play.api.libs.json.Json
import play.api.mvc.{AbstractController, ControllerComponents}
import services.ProductService

import scala.concurrent.{ExecutionContext, Future}


@Singleton
class ProductController @Inject()(cc: ControllerComponents,
                                  service: ProductService)(implicit ec: ExecutionContext) extends AbstractController(cc) {

  def create = Action.async(parse.json[ProductModels.Product]) { implicit request =>

    service.create(request.body).map { product =>
      Created( Json.toJson(product) )
    }

  }

  def update(id: Int) = Action.async(parse.json[ProductModels.Product]) { implicit request =>

    service.update(request.body.copy(id = Some(id))).map { product =>
      Ok( Json.toJson(product) )
    }

  }


  def list = Action.async { implicit request =>

    service.list map { seq =>
      Ok( Json.toJson(seq) )
    }

  }

  def get(id: Int) = Action.async { implicit request =>

    service.get(id) map {
      case Some(p) => Ok(Json.toJson(p))
      case None => NotFound
    }

  }

  def delete(id: Int) = Action.async { implicit request =>

    service.delete(id) map { _ =>
      Ok
    }

  }
}
