lazy val root = (project in file(".")).aggregate(schema, backend)

lazy val schema = project

lazy val backend = project.enablePlugins(PlayScala)

// common settings

scalaVersion in ThisBuild := "2.12.2"

resolvers in ThisBuild ++= Seq(
  "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases",
  "Akka Snapshot Repository" at "http://repo.akka.io/snapshots/"
)

