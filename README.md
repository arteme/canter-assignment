# Canter FullStack Recruitment Task Solution

  This repository contains a solution to the [Canter FullStack Recruitment
  Task](https://bitbucket.org/canterpublic/full-stack-dev-recruitement-task)

  The solution is implemented in:
  
  * Backend: Scala 2.12 with Play 2.6 and Slick 3.2;
  * Frontend: AngularJS 1.7.5.
  
  Backend code can be found from `backend/` folder (`backend` sub-project).
  Frontend code can be found from `backend/public/` folder. Frontend is
  served by the Play application.
  
  Additionally, schema management code can be found from `schema/` folder
  (`schema` sub-project).
  
## Schema Management

  The code uses Flyway to facilitate schema management and `slick-codegen`
  for generating `Tables.scala` from the database schema for the `backend`
  project. The schema migration SQL scripts are located in the
  `schema/src/main/db/migration/` folder. The configuration for running
  migration and code generation can be found from the
  `schema/src/main/resources/schema.conf` file. This configuration
  duplicates the database configuration used by the `backend` project.
  
  Database migration can be ran from command line:
  
    sbt migrateDb
    
  Schema generation can be ran from command line:
  
    sbt schemaGen
    
  Schema was developed MySQL syntax (using MySQL profile in Slick)
  and tested with MariaDB 10.1.
    
## Backend

  Play application configuration can be found from
  `backend/conf/application.conf` file. For development purposes
  "allowed hosts" and "security headers" filters are turned off.
  Slick integration is done using `play-slick` library, with configuration
  at the bottom of the play application configuration file.

  Backend can be started in dev mode from command line:
  
    sbt compile backend/run
    
  The application can then be reached at `http://127.0.0.1:9000`
  address.