CREATE TABLE `detail` (
  `id` INTEGER AUTO_INCREMENT NOT NULL,
  `product_id` INTEGER NOT NULL,
  `key` VARCHAR (128) NOT NULL,
  `value` VARCHAR (128) NOT NULL,

  PRIMARY KEY (`id`),

  CONSTRAINT `fk__product_id`
    FOREIGN KEY (`product_id`)
    REFERENCES `product` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,

  UNIQUE INDEX `idx__product_and_key` (`product_id` ASC, `key` ASC)

) ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8
  COLLATE = utf8_general_ci;
