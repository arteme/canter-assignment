import com.typesafe.config.ConfigFactory
import slick.basic.DatabaseConfig
import slick.codegen.SourceCodeGenerator
import slick.jdbc.JdbcProfile

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.util.Try


object SchemaGen extends App {


  val config = ConfigFactory.load("schema")
  //val db = Database.forConfig("db", config)

  val dbConfig = DatabaseConfig.forConfig[JdbcProfile]("sql", config)
  val profile = dbConfig.profile

  val modelAction = profile.createModel(
    Some(profile.defaultTables.map(
      _.filter(!_.name.name.toLowerCase.endsWith("schema_version"))
    ))
  )
  val modelFuture = dbConfig.db.run(modelAction)
  val codegenFuture = modelFuture.map { model => new SourceCodeGenerator(model) {
    }
  }

  val fileWriteFuture = codegenFuture.map { codegen =>
    val targetDir = Try(args(0)).getOrElse {
      System.err.println("usage: schemaGen <target folder>")
      System.exit(1)
      ""
    }

    println(s"Generating Tables.scala for profile: ${dbConfig.profileName}")
    codegen.writeToFile(dbConfig.profileName, targetDir, "db", "Tables", "Tables.scala")
  }

  Await.result(fileWriteFuture, 3 minutes)
}
