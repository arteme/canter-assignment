import com.typesafe.config.ConfigFactory
import org.flywaydb.core.Flyway
import org.flywaydb.core.api.MigrationVersion

object Migration extends App {

  val config = ConfigFactory.load("schema")
  val url = config.getString("sql.db.url")
  val user = config.getString("sql.db.user")
  val password = config.getString("sql.db.password")

  val flyway = new Flyway
  flyway.setDataSource(url, user, password)

  val current = Option(flyway.info().current())
  val version = current.map(_.getVersion).getOrElse(MigrationVersion.EMPTY)

  println(s"Current DB migration version: $version")

  if(flyway.info().pending().isEmpty) {
    println("Instance up to date!")
    System.exit(1)
  }

  println("Pending migrations:")
  flyway.info().pending().foreach { mi =>
    println(s"- ${mi.getVersion} : ${mi.getDescription}")
  }

  println(s"Target DB migration version: ${flyway.getTarget}")

  flyway.migrate()
}
