
libraryDependencies ++= Seq(
  "com.typesafe.slick" %% "slick" % "3.2.0",
  "com.typesafe.slick" %% "slick-codegen" % "3.2.0",
  "com.typesafe" % "config" % "1.3.0",
  "org.flywaydb" % "flyway-core" % "3.1",
  "org.mariadb.jdbc" % "mariadb-java-client" % "1.3.7"
)


// define a task to generate the schema
lazy val schemaGen = taskKey[Unit]("Generate Tables.scala from the database")
schemaGen := Def.taskDyn {
  val targetPath = (baseDirectory.value / ".." / "backend" / "app").absolutePath
  Def.task {
    (runMain in Compile).toTask(s" SchemaGen $targetPath").value
  }
}.value

// define a task to run Flyway migration
lazy val migrateDb = taskKey[Unit]("Run Flyway migration on configured database")
migrateDb := Def.task {
  (runMain in Compile).toTask(s" Migration").value
}.value
